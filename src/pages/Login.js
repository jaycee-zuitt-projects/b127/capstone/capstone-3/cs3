import { Fragment, useState, useContext, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
//user context
import UserContext from '../UserContext';
//Routing
import { Redirect, useHistory } from 'react-router-dom';

const Login = () => {

    const history = useHistory();

    const { user, setUser } = useContext(UserContext)

    const [ email, setEmail ] = useState('');
    const [ password, setPassword ] = useState('');
    const [ isActive, setIsActive ] = useState(false);

    useEffect(() => {
        if((email !== "" && password !== "")){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password])

    function loginUser(e){
        e.preventDefault();

        fetch(`${ process.env.REACT_APP_API_URL }/users/login`, {
            method: "POST",
            headers:{
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data.accessToken !== undefined){
                localStorage.setItem('accessToken', data.accessToken)
                setUser({ accessToken: data.accessToken });
                Swal.fire({
                    title: "Enjoy Shopping!!!!",
                    icon: "success",
                    text: "You have successfully login"
                })
                setEmail("")
                setPassword("")

                fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    if(data.isAdmin === true){
                        localStorage.setItem('email', data.email)
                        localStorage.setItem('isAdmin', data.isAdmin)
                        setUser({
                            email: data.email,
                            isAdmin: data.isAdmin
                        })

                        history.push('/products')
                    }else{
                        history.push('/')
                    }
                })
            }else{
                Swal.fire({
                    title: "Something Went Wrong",
                    icon: "error",
                    text: "Please check your credentials."
                })
            }
        })
    }


    return (
        (user.accessToken !== null)?
            <Redirect to="/" />
            :
        <Fragment>
            <Form>
                <h1>User Login</h1>
                <Form.Group className="mb-3">
                    <Form.Label>Email Address:</Form.Label>
                    <Form.Control type="email" placeholder="Enter Email" />
                </Form.Group>
                <Form.Group className="mb-3">
                    <Form.Label>Password:</Form.Label>
                    <Form.Control type="password" placeholder="Enter Password" />
                </Form.Group>
                {isActive ?
                <Button variant="primary" type="submit" id="submitBtn">Login</Button>
                :
                <Button variant="primary" type="submit" id="submitBtn" disabled>Login</Button>
                }
            </Form>
            
        </Fragment>
    )
}

export default Login
