import { Carousel, Button } from 'react-bootstrap'

const Banner = () => {
    return (
        <Carousel>
            <Carousel.Item>
                <img className="d-block w-100" src="./images/website-images-800-x-400-px-1.png" alt="First Slide" />
                <Carousel.Caption>
                    <h3>Allusion Services</h3>
                    <p>Allusion Company offers different services</p>
                    <Button variant="secondary">Know More!!!</Button>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src="./images/website-images-800-x-400-px-1.png" alt="Second Slide" />
                <Carousel.Caption>
                    <h3>Allusion Services</h3>
                    <p>Allusion Company offers different services</p>
                    <Button variant="secondary">Know More!!!</Button>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img className="d-block w-100" src="./images/website-images-800-x-400-px-1.png" alt="Third Slide" />
                <Carousel.Caption>
                    <h3>Allusion Services</h3>
                    <p>Allusion Company offers different services</p>
                    <Button variant="secondary">Know More!!!</Button>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    )
}

export default Banner
